# SaQC Environment

Try out SaQC yourself, we've already installed all prerequisites.. 

Helpful links
- Docs: https://rdm-software.pages.ufz.de/saqc/
- Repo: https://git.ufz.de/rdm-software/saqc
- PyPi: https://pypi.org/project/saqc/
