# saqcEnv

This is a simple repro for https://mybinder.org/, because our saqc repro with all its history in commits is way to big to compile there reliable. 

Any file ion this repro will be in the final JupiterLab.. 

WOrkspaces are available for:
- (lastest) [![PyPI version](https://badge.fury.io/py/saqc.svg)](https://badge.fury.io/py/saqc): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.ufz.de%2Fpalmb%2Fsaqcenv/main)

- SaQC@develop: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.ufz.de%2Fpalmb%2Fsaqcenv/develop)